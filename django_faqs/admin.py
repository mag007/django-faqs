from django.contrib import admin
from models import Question, Section
from django.contrib.admin import TabularInline

class QuestionInline(TabularInline):
    model = Question

class SectionAdmin(admin.ModelAdmin):
    inlines = (QuestionInline,)

    class Media:
        css = {
            "all": (
                "compiled-css/admin.css",
                "css/bootstrap-wysihtml5.css",
            )
        }
        js = (
            'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
            'js-lib/jquery.livequery.js',
            'js-lib/bootstrap-modal.js',
            'js-lib/wysihtml5-0.3.0.js',
            'js-lib/bootstrap-wysihtml5.js',
            'js/admin/textareas.js',
            )

admin.site.register(Section, SectionAdmin)