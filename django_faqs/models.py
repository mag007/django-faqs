from django.db import models

class Section(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return u'%s' % (self.name)


class Question(models.Model):
    section = models.ForeignKey(Section)
    question = models.CharField(max_length=200)
    answer = models.TextField()