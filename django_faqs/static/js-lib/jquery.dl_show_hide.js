jQuery(function() {
    "use strict";

    // If it's in a tab, unhide it
    jQuery('.dl-show-hide').closest('.tab-pane').css('display','block');

    jQuery('.dl-show-hide dt+dd').each(function(){
        var $dd = jQuery(this);
        $dd.addClass('active');
        var h = $dd.height();
        var p = parseInt($dd.css('padding-top'), 10);
        $dd.attr('data-expanded-height',h);
        $dd.attr('data-expanded-vert-padding',p);
        $dd.removeClass('active')
            .css({'height':0,'padding-top':0,'padding-bottom':0, 'overflow':'hidden'});
    });

    // Rehide tabs
    jQuery('.dl-show-hide').closest('.tab-pane').css('display','');

    // Style it with the cursor icon
    jQuery('.dl-show-hide dt').css('cursor', 'pointer');

    // Handle our definition title click
    jQuery('.dl-show-hide dt').toggle(function() {
        jQuery(this).addClass('active');
        var $dd = jQuery(this).next('dd');
        var h = $dd.attr('data-expanded-height');
        var p = $dd.attr('data-expanded-vert-padding');
        $dd.addClass('active')
            .css({'height':0,'padding-top':0,'padding-bottom':0})
            .animate({height:h,'padding-top':p,'padding-bottom':p});
    },function() {
        jQuery(this).removeClass('active');
        var $dd = jQuery(this).next('dd');
        $dd.removeClass('active')
            .animate({height:0,'padding-top':0,'padding-bottom':0});
    });
});