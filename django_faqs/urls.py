from django.conf.urls.defaults import *
from views import IndexView

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view()),
)