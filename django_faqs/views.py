from django.views.generic import TemplateView
from models import Section, Question
import string

class IndexView(TemplateView):
    template_name = 'faqs/index.html'

    def get_context_data(self, **kwargs):
        return {'sections': Section.objects.all()}

    def render_to_response(self, context, **response_kwargs):
        if 'app_config' in getattr(self.request, '_feincms_extra_context', {}):
            return self.get_template_names(), context

        return super(IndexView, self).render_to_response(context, **response_kwargs)