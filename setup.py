from distutils.core import setup

setup(
    name='django-faqs',
    version='0.1.0',
    author='Magnetic Creative',
    author_email='dev@mag.cr',
    packages=['django_faqs'],
    license='LICENSE.txt',
    description='FAQs app.',
    long_description=open('README.rst').read(),
    install_requires=[
        "Django >= 1.1.1",
        "django-sekizai == 0.6.1",
    ],
)